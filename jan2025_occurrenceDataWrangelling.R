library(geosphere)

dmasechGm5 = c(134.2349, 7.1292)

hungerOcc = read.table(file = "18.xii.2024_hungerfordia_dets2024_latLongs.txt", header = T, sep = "\t")

hungerOcc$distFromDmasech = distGeo(cbind(hungerOcc$long, hungerOcc$lat), dmasechGm5)


library(tidyverse)

write_delim(hungerOcc, "20.i.2025_hungerfordia_dets2024_latLongs_dist2Dmasech.txt", quote = "none", eol = "\n", delim = "\t")


### summarize islands/outcrops


justOutcropsAndRegions = as_tibble(cbind(Island = hungerOcc$Island, IslandGroup = hungerOcc$IslandGroup))

justOutcropsAndRegions  = justOutcropsAndRegions %>% distinct(Island, IslandGroup, .keep_all = TRUE)

hungerOccOutcrops = hungerOcc %>% 
	group_by(Island) %>% 
	select(Locality, lat, long, species_cf, species_lumped, species_subspp, distFromDmasech) %>%
	summarise(
	localitiesCount = n_distinct(Locality),
	richness_cf = n_distinct(species_cf), 
	richness_lumped = n_distinct(species_lumped), 
	richness_subspp = n_distinct(species_subspp),
	mean_lat = mean(lat), 
	mean_long = mean(long), 
	mean_dist2Dmasech = mean(distFromDmasech)
	) 
	
	
hungerOccOutcrops =	left_join(hungerOccOutcrops, justOutcropsAndRegions)

write_delim(hungerOccOutcrops, "20.i.2025_hungerfordia_outcropSummaries.txt", quote = "none", eol = "\n", delim = "\t")
	
	
hungerOccIslandGroups = hungerOcc %>% 
	group_by(IslandGroup) %>% 
	select(Island, Locality, lat, long, species_cf, species_lumped, species_subspp, distFromDmasech) %>%
	summarise(
	islandCount = n_distinct(Island),
	localitiesCount = n_distinct(Locality),
	richness_cf = n_distinct(species_cf), 
	richness_lumped = n_distinct(species_lumped), 
	richness_subspp = n_distinct(species_subspp),
	mean_lat = mean(lat), 
	mean_long = mean(long), 
	mean_dist2Dmasech = mean(distFromDmasech)
	) 


write_delim(hungerOccIslandGroups, "20.i.2025_hungerfordia_islandGroupSummaries.txt", quote = "none", eol = "\n", delim = "\t")

outcropAreas = read.table(file = "20.i.2025_hungerfordia_outcropSummaries_plusAreas.txt", header = T, sep = "\t")

areasIslandGroups = outcropAreas %>%
	group_by(IslandGroup) %>% 
	select(areaSqM) %>%
	summarise(totalAreaSqM = sum(areaSqM))
	
hungerOccIslandGroups = left_join(hungerOccIslandGroups, areasIslandGroups)



hungerSpeciesLumped_summaries = hungerOcc %>% 
	group_by(species_lumped) %>% 
	select(Locality, IslandGroup, Island, distFromDmasech) %>%
	summarise(
	localitiesCount = n_distinct(Locality),
	islandCount = n_distinct(Island), 
	islandGroupCount = n_distinct(IslandGroup),
	minDist2Dmasech = min(distFromDmasech), 
	maxDist2Dmasech = max(distFromDmasech),
	meanDist2Dmasech = mean(distFromDmasech), 
	airaiCount = sum(IslandGroup=="Airai")/31, 
	kororCount = sum(IslandGroup=="Koror")/34, 
	ngeruktabelCount = sum(IslandGroup =="Ngeruktabel")/43, 
	southernCount = sum(IslandGroup == "Southern")/126
	) 
	
	
write_delim(hungerSpeciesLumped_summaries, "20.i.2025_hungerfordia_dets2024_hungerSpeciesLumped_summaries.txt", quote = "none", eol = "\n", delim = "\t")



justLocalitiesAndIslands = as_tibble(cbind(Island = hungerOcc$Island, Locality = hungerOcc$Locality)) %>%
	distinct(Locality, Island, .keep_all = TRUE)


hungerOccLocalitySummaries = hungerOcc %>% 
	group_by(Locality) %>% 
	select(species_cf, species_lumped, species_subspp, distFromDmasech, lat, long) %>%
	summarise(
	richness_cf = n_distinct(species_cf), 
	richness_lumped = n_distinct(species_lumped), 
	richness_subspp = n_distinct(species_subspp),
	lat = mean(lat), 
	long = mean(long), 
	dist2Dmasech = mean(distFromDmasech)
	) 
	
hungerOccLocalitySummaries = left_join(hungerOccLocalitySummaries, justLocalitiesAndIslands)

hungerOccLocalitySummaries = left_join(hungerOccLocalitySummaries, outcropAreas, by = "Island")

write_delim(hungerOccLocalitySummaries, "21.i.2025_hungerfordia_locality_Summaries.txt", quote = "none", eol = "\n", delim = "\t")




library(ggplot2)
library(ggridges)
### need to fix this below... might want to just filter for only species with >2 occurrences? 

ggplot(hungerOcc, aes(x = distFromDmasech, y = species_lumped, color = species_lumped, fill = species_lumped)) +
    geom_density_ridges(
    jittered_points = TRUE, scale = .95, rel_min_height = .01,
    point_shape = "|", point_size = 3, size = 0.25,
    position = position_points_jitter(height = 0)) + 
    scale_y_discrete(limits = c("chilorhytis", "ringens", "loxodonta", "crenata", "goniobasis", "pyramis", "inflatula", "papilio", "brachyptera", "triplochilus", "spiroperculata", "eurystoma", "nodulosa", "omphaloptyx", "elegantissima", "polymorpha", "nudicollum", "lutea", "spinoscapula", "robiginosa", "subalata", "fragilipennis", "pelewensis", "crassilabris", "longissima", "globosa", "alata", "lamellata", "aurea", "microbasodonta", "basodonta", "aspera", "pteropurpuroides", "ngereamensis", "rudicostata", "expansilabris", "unisulcata", "echinata", "irregularis")) + 


     scale_fill_manual(values=c("purple", "purple", "purple", "purple", "purple", "purple", "purple", "blue", "purple", "blue", "purple", "blue", "purple", "blue", "blue", "turquoise", "blue", "blue", "turquoise", "blue", "blue", "blue", "turquoise", "turquoise", "darkgreen", "darkgreen", "darkgreen", "darkgreen", "darkgreen", "turquoise", "darkgreen", "goldenrod", "goldenrod", "goldenrod", "goldenrod", "goldenrod", "goldenrod", "goldenrod", "goldenrod")) + 
    
    scale_color_manual(values=c("purple", "purple", "purple", "purple", "purple", "purple", "purple", "blue", "purple", "blue", "purple", "blue", "purple", "blue", "blue", "turquoise", "blue", "blue", "turquoise", "blue", "blue", "blue", "turquoise", "turquoise", "darkgreen", "darkgreen", "darkgreen", "darkgreen", "darkgreen", "turquoise", "darkgreen", "goldenrod", "goldenrod", "goldenrod", "goldenrod", "goldenrod", "goldenrod", "goldenrod", "goldenrod")) + 
   
	theme_minimal()
  

### just a big loop to explore how redundant (or not) 
### different beta diversity metrics are with respect to each other

### ultimately, I think this is useful to show difference in spatial-autocorrelation 
### between geographic groupings (see plots at the end)


library(vegan)
library(sp)
library(geosphere)

combinedSppSiteMatrix = cbind(localityLumpedSppSites, localityLumpedSppMatrix)

betaDiversityComparisonSitesSppLumpedNames = c("raup", "jaccard", "chao", "bray", "cao", "binomial", "horn", "kulczynski", "clark", "gower", "sameIslandGroup", "sameIsland", "distKM", "sumRichness", "diffRichness", "maxRichness", "minRichness", "localities", "islands", "islandGroups")
betaDiversityComparisonSitesSppLumped = data.frame(matrix(ncol = 17, nrow = 0))

for (i in 1:(dim(combinedSppSiteMatrix)[1] - 1)){
	for (j in (i + 1):(dim(combinedSppSiteMatrix)[1])){
		twoSites = rbind(combinedSppSiteMatrix[i, ], combinedSppSiteMatrix[j, ])
		raupDist = vegdist(twoSites[ , 11:49], method = "raup")
		jacDist = vegdist(twoSites[ , 11:49], method = "jaccard", binary = TRUE)
		chaoDist = vegdist(twoSites[ , 11:49], method = "chao", binary = TRUE)
		brayDist = vegdist(twoSites[ , 11:49], method = "bray", binary = TRUE)
		caoDist = vegdist(twoSites[ , 11:49], method = "cao", binary = TRUE)
		binomialDist = vegdist(twoSites[ , 11:49], method = "binomial", binary = TRUE)
		hornDist = vegdist(twoSites[ , 11:49], method = "horn", binary = TRUE)
		kulczynskiDist = vegdist(twoSites[ , 11:49], method = "kulczynski", binary = TRUE)
		clarkDist = vegdist(twoSites[ , 11:49], method = "clark", binary = TRUE)
		gowerDist = vegdist(twoSites[ , 11:49], method = "gower", binary = TRUE)
		sameIslandGroup = twoSites[1, 9] == twoSites[2, 9]
		sameIsland = twoSites[1, 8] == twoSites[2, 8]
		distKM = distGeo(twoSites[1 , 6:5], twoSites[2 , 6:5])/1000
		sumRichness = twoSites[1, 3] + twoSites[2, 3]
		diffRichness = abs(twoSites[1, 3] - twoSites[2, 3])
		maxRichness = max(twoSites[ , 3])
		minRichness = min(twoSites[ , 3])
		localities = paste(sort(c(twoSites[1, 1], twoSites[2, 1])), collapse = "_")
		islands = paste(sort(c(twoSites[1, 8], twoSites[2, 8])), collapse = "_")
		islandGroups = paste(sort(c(twoSites[1, 9], twoSites[2, 9])), collapse = "_")
		outputRow = c(raupDist, jacDist, chaoDist, brayDist, caoDist, binomialDist, hornDist, kulczynskiDist, clarkDist, gowerDist, sameIslandGroup, sameIsland, distKM, sumRichness, diffRichness, maxRichness, minRichness, localities, islands, islandGroups)
		betaDiversityComparisonSitesSppLumped = rbind(betaDiversityComparisonSitesSppLumped, outputRow)
	}
}

colnames(betaDiversityComparisonSitesSppLumped) = betaDiversityComparisonSitesSppLumpedNames

write.table(betaDiversityComparisonSitesSppLumped, file = "19.ii.2025_ betaDiversityComparison_localities_SppLumped.csv", sep = ",",  col.names = TRUE, row.names = FALSE, append=FALSE, quote = FALSE)

betaCompsSitesSppLumped = read.table(file = "19.ii.2025_ betaDiversityComparison_localities_SppLumped.csv", header = T ,sep = ",")

library(ggplot2)
library(viridis)

pdf(file = "21.ii.2025_sppLumped_spatialAutocorr.pdf", onefile = TRUE, width = 11, height = 8)

ggplot(betaCompsSitesSppLumped, aes(y = raup, x =(distKM), col = as.character(sameIslandGroup + sameIsland))) +
	theme_minimal() + 
	geom_point(size = 1, alpha = 0.3) + 
	scale_color_viridis_d(name = "Geography", labels = c("Different Groups", "Same Group", "Same Island")) + 
	geom_smooth(data = betaCompsSitesSppLumped, aes(y = raup, x =(distKM)), method = "loess") + 
	scale_x_continuous(name = "Distance in Km") + 
	scale_y_continuous(name = "Raup-Crick distance") +
	labs(title = "Beta diversity comparison", subtitle = "Based on presence-absence data") 
	

ggplot(betaCompsSitesSppLumped, aes(y = chao, x =(distKM), col = as.character(sameIslandGroup + sameIsland))) +
	theme_minimal() + 
	geom_point(size = 1, alpha = 0.3) + 
	scale_color_viridis_d(name = "Geography", labels = c("Different Groups", "Same Group", "Same Island")) + 
	geom_smooth(data = betaCompsSitesSppLumped, aes(y = chao, x =(distKM)), method = "loess") + 
	scale_x_continuous(name = "Distance in Km") + 
	scale_y_continuous(name = "Chao distance") +
	labs(title = "Beta diversity comparison", subtitle = "Based on presence-absence data") 


ggplot(betaCompsSitesSppLumped, aes(y = jaccard, x =(distKM), col = as.character(sameIslandGroup + sameIsland))) +
	theme_minimal() + 
	geom_point(size = 1, alpha = 0.3) + 
	scale_color_viridis_d(name = "Geography", labels = c("Different Groups", "Same Group", "Same Island")) + 
	geom_smooth(data = betaCompsSitesSppLumped, aes(y = jaccard, x =(distKM)), method = "loess") + 
	scale_x_continuous(name = "Distance in Km") + 
	scale_y_continuous(name = "Jaccard distance") +
	labs(title = "Beta diversity comparison", subtitle = "Based on presence-absence data") 

ggplot(betaCompsSitesSppLumped, aes(y = bray, x =(distKM), col = as.character(sameIslandGroup + sameIsland))) +
	theme_minimal() + 
	geom_point(size = 1, alpha = 0.3) + 
	scale_color_viridis_d(name = "Geography", labels = c("Different Groups", "Same Group", "Same Island")) + 
	geom_smooth(data = betaCompsSitesSppLumped, aes(y = bray, x =(distKM)), method = "loess") + 
	scale_x_continuous(name = "Distance in Km") + 
	scale_y_continuous(name = "Bray-Curtis distance") +
	labs(title = "Beta diversity comparison", subtitle = "Based on presence-absence data") 

ggplot(betaCompsSitesSppLumped, aes(y = cao, x =(distKM), col = as.character(sameIslandGroup + sameIsland))) +
	theme_minimal() + 
	geom_point(size = 1, alpha = 0.3) + 
	scale_color_viridis_d(name = "Geography", labels = c("Different Groups", "Same Group", "Same Island")) + 
	geom_smooth(data = betaCompsSitesSppLumped, aes(y = cao, x =(distKM)), method = "loess") + 
	scale_x_continuous(name = "Distance in Km") + 
	scale_y_continuous(name = "Cao distance") +
	labs(title = "Beta diversity comparison", subtitle = "Based on presence-absence data") 

ggplot(betaCompsSitesSppLumped, aes(y = binomial, x =(distKM), col = as.character(sameIslandGroup + sameIsland))) +
	theme_minimal() + 
	geom_point(size = 1, alpha = 0.3) + 
	scale_color_viridis_d(name = "Geography", labels = c("Different Groups", "Same Group", "Same Island")) + 
	geom_smooth(data = betaCompsSitesSppLumped, aes(y = binomial, x =(distKM)), method = "loess") + 
	scale_x_continuous(name = "Distance in Km") + 
	scale_y_continuous(name = "Binomial distance") +
	labs(title = "Beta diversity comparison", subtitle = "Based on presence-absence data") 

ggplot(betaCompsSitesSppLumped, aes(y = horn, x =(distKM), col = as.character(sameIslandGroup + sameIsland))) +
	theme_minimal() + 
	geom_point(size = 1, alpha = 0.3) + 
	scale_color_viridis_d(name = "Geography", labels = c("Different Groups", "Same Group", "Same Island")) + 
	geom_smooth(data = betaCompsSitesSppLumped, aes(y = horn, x =(distKM)), method = "loess") + 
	scale_x_continuous(name = "Distance in Km") + 
	scale_y_continuous(name = "Horn-Morisita distance") +
	labs(title = "Beta diversity comparison", subtitle = "Based on presence-absence data") 

ggplot(betaCompsSitesSppLumped, aes(y = kulczynski, x =(distKM), col = as.character(sameIslandGroup + sameIsland))) +
	theme_minimal() + 
	geom_point(size = 1, alpha = 0.3) + 
	scale_color_viridis_d(name = "Geography", labels = c("Different Groups", "Same Group", "Same Island")) + 
	geom_smooth(data = betaCompsSitesSppLumped, aes(y = kulczynski, x =(distKM)), method = "loess") + 
	scale_x_continuous(name = "Distance in Km") + 
	scale_y_continuous(name = "Kulczynski distance") +
	labs(title = "Beta diversity comparison", subtitle = "Based on presence-absence data") 

ggplot(betaCompsSitesSppLumped, aes(y = clark, x =(distKM), col = as.character(sameIslandGroup + sameIsland))) +
	theme_minimal() + 
	geom_point(size = 1, alpha = 0.3) + 
	scale_color_viridis_d(name = "Geography", labels = c("Different Groups", "Same Group", "Same Island")) + 
	geom_smooth(data = betaCompsSitesSppLumped, aes(y = clark, x =(distKM)), method = "loess") + 
	scale_x_continuous(name = "Distance in Km") + 
	scale_y_continuous(name = "Clark distance") +
	labs(title = "Beta diversity comparison", subtitle = "Based on presence-absence data") 

ggplot(betaCompsSitesSppLumped, aes(y = gower, x =(distKM), col = as.character(sameIslandGroup + sameIsland))) +
	theme_minimal() + 
	geom_point(size = 1, alpha = 0.3) + 
	scale_color_viridis_d(name = "Geography", labels = c("Different Groups", "Same Group", "Same Island")) + 
	geom_smooth(data = betaCompsSitesSppLumped, aes(y = gower, x =(distKM)), method = "loess") + 
	scale_x_continuous(name = "Distance in Km") + 
	scale_y_continuous(name = "Gower distance") +
	labs(title = "Beta diversity comparison", subtitle = "Based on presence-absence data") 

plot(betaCompsSitesSppLumped[, 1:10], pch = ".", main = "Beta diversity indices as functions of each other")

dev.off()